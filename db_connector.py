print 'Greetings!'
print '''

Database Connection : Python-MySQL
----------------------------------
1. You have created a database TESTDB.
2. You have created a table EMPLOYEE in TESTDB.
3. This table has fields FIRST_NAME, LAST_NAME, AGE, SEX and INCOME.
4. User ID "testuser" and password "test123" are set to access TESTDB.
5. Python module MySQLdb is installed properly on your machine.
6. You have gone through MySQL tutorial to understand MySQL Basics.


If you lose this password, please consult the section How to Reset the Root Password in the MySQL reference manual.
ALTER USER 'root'@'localhost' IDENTIFIED BY 'root';         #5.6
SET PASSWORD FOR 'root'@'localhost' = PASSWORD('root');     #5.7

'''

import MySQLdb
db = MySQLdb.connect("localhost","root","","python_my" )
cursor = db.cursor()
cursor.execute("SELECT VERSION()")
data = cursor.fetchone()
print "Database version is : %s " % data


#tutorial 2
cursor.execute("DROP TABLE IF EXISTS EMPLOYEE")
# Create table as per requirement
sql = """CREATE TABLE EMPLOYEE (
         FIRST_NAME  CHAR(20) NOT NULL,
         LAST_NAME  CHAR(20),
         AGE INT,
         SEX CHAR(1),
         INCOME FLOAT )"""

#tutorial 3
# Prepare SQL query to INSERT a record into the database.
insert_sql = """INSERT INTO EMPLOYEE(FIRST_NAME,
         LAST_NAME, AGE, SEX, INCOME)
         VALUES ('Mac', 'Mohan', 20, 'M', 2000)"""

cursor.execute(insert_sql)

#tutorial 4
# Prepare SQL query to INSERT a record into the database.
# Prepare SQL query to INSERT a record into the database.

# sql = "SELECT * FROM EMPLOYEE \
#        WHERE INCOME > '%d'" % (1000)
# try:
#     # Execute the SQL command
#     cursor.execute(sql)
#     # Fetch all the rows in a list of lists.
#     results = cursor.fetchall()
#     print results
#     for row in results:
#         fname = row[0]
#         lname = row[1]
#         age = row[2]
#         sex = row[3]
#         income = row[4]
#         # Now print fetched result
#         print '\n\n\fetching record from database.\n'
#         print "fname=%s,lname=%s,age=%d,sex=%s,income=%d" % \
#              (fname, lname, age, sex, income )
#         print
# except Exception as e:
#     print "Error: unable to fecth data"


#tutorial 5
# Prepare SQL query to UPDATE required records
sql = "UPDATE EMPLOYEE SET AGE = AGE + 1 WHERE SEX = '%c'" % ('M')
try:
    # Execute the SQL command
    cursor.execute(sql)
    # Commit your changes in the database
    db.commit()
except Exception as e:
    # Rollback in case there is any error
    db.rollback()

cursor.execute(sql)
db.close()


#turorial 6
#!/usr/bin/python
# Open database connection

# Prepare SQL query to DELETE required records
# sql = "DELETE FROM EMPLOYEE WHERE AGE > '%d'" % (20)
# try:
#     # Execute the SQL command
#     cursor.execute(sql)
#     # Commit your changes in the database
#     db.commit()
# except:
#     # Rollback in case there is any error
#     db.rollback()

# disconnect from server
db.close()

# export PATH=/usr/local/mysql/bin:$PATH
