import time
import sys

def cli_progress_test(end_val, bar_length=100):
    print '\n TIMER %d sec\n'%(end_val)
    print ''
    for i in xrange(0, end_val):
        time.sleep(1)
        percent = float(i) / end_val
        hashes = '#' * int(round(percent * bar_length))
        spaces = ' ' * (bar_length - len(hashes))
        sys.stdout.write("\rPROGRESS : [{0}] {1}%".format(hashes + spaces, int(round(percent * 100))))
        sys.stdout.flush()
    print '\n TIMEOUT \n'

cli_progress_test(120)
